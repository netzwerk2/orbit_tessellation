use bevy::diagnostic::FrameTimeDiagnosticsPlugin;
use bevy::prelude::*;
use bevy::window::PresentMode;
use bevy_polyline::prelude::*;

use orbit_tessellation::{
    OrbitBundle, OrbitTessellationDiagnosticsPlugin, OrbitTessellationPlugin, PolylineMaterial,
    TessellationCamera,
};

use crate::overlay::OrbitDebugPlugin;
use crate::settings_ui::SettingsUiPlugin;

mod overlay;
mod settings_ui;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::BLACK))
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "Orbit Tessellation Debug UI Example".to_string(),
                present_mode: PresentMode::Immediate,
                ..default()
            }),
            ..default()
        }))
        .add_plugins((OrbitTessellationPlugin, DebugOverlayPlugin))
        .add_systems(Startup, setup)
        .run();
}

struct DebugOverlayPlugin;

impl Plugin for DebugOverlayPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            FrameTimeDiagnosticsPlugin,
            OrbitTessellationDiagnosticsPlugin,
            OrbitDebugPlugin,
            SettingsUiPlugin,
        ));
    }
}

fn setup(
    mut commands: Commands,
    mut polylines: ResMut<Assets<Polyline>>,
    mut materials: ResMut<Assets<PolylineMaterial>>,
) {
    commands
        .spawn(Camera3dBundle {
            transform: Transform::from_translation(Vec3::new(
                200_000_000_000.0,
                200_000_000_000.0,
                200_000_000_000.0,
            ))
            .looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        })
        .insert(TessellationCamera);

    commands.spawn(OrbitBundle {
        polyline_bundle: PolylineBundle {
            polyline: polylines.add(Polyline::default()),
            material: materials.add(PolylineMaterial {
                width: 1.0,
                ..default()
            }),
            transform: Transform::from_scale(Vec3::ONE / 5.0),
            ..default()
        },
        ..default()
    });
}
