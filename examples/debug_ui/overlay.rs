use bevy::diagnostic::{DiagnosticsStore, FrameTimeDiagnosticsPlugin};
use bevy::prelude::*;
use bevy_polyline::prelude::*;

use orbit_tessellation::{OrbitTessellationDiagnosticsPlugin, TessellationSettings};

use crate::settings_ui::DebugSettings;

const POINT_VERTICES: [Vec3; 8] = [
    Vec3::X,
    Vec3::NEG_X,
    Vec3::splat(f32::INFINITY),
    Vec3::Y,
    Vec3::NEG_Y,
    Vec3::splat(f32::INFINITY),
    Vec3::Z,
    Vec3::NEG_Z,
];

pub struct OrbitDebugPlugin;

impl Plugin for OrbitDebugPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup).add_systems(
            Update,
            (update_diagnostics_text, draw_triangles, draw_points),
        );
    }
}

#[derive(Component)]
struct StatsText;

#[derive(Component, Deref, DerefMut)]
struct StatsTimer(Timer);

#[derive(Component)]
pub struct TrianglesPolyline;

#[derive(Component)]
pub struct PointsPolyline;

fn setup(
    mut commands: Commands,
    mut polyline_materials: ResMut<Assets<PolylineMaterial>>,
    mut polylines: ResMut<Assets<Polyline>>,
) {
    let text_style = TextStyle {
        font_size: 16.0,
        color: Color::WHITE,
        ..default()
    };
    commands
        .spawn(TextBundle {
            style: Style {
                align_self: AlignSelf::FlexEnd,
                position_type: PositionType::Absolute,
                ..default()
            },
            text: Text {
                sections: vec![
                    TextSection {
                        value: String::from("FPS: "),
                        style: text_style.clone(),
                    },
                    TextSection {
                        style: text_style.clone(),
                        ..default()
                    },
                    TextSection {
                        value: String::from("\nOrbits: "),
                        style: text_style.clone(),
                    },
                    TextSection {
                        style: text_style.clone(),
                        ..default()
                    },
                    TextSection {
                        value: String::from("\nInitial Points: "),
                        style: text_style.clone(),
                    },
                    TextSection {
                        style: text_style.clone(),
                        ..default()
                    },
                    TextSection {
                        value: String::from("\nPoints: "),
                        style: text_style.clone(),
                    },
                    TextSection {
                        style: text_style,
                        ..default()
                    },
                ],
                ..default()
            },
            ..default()
        })
        .insert(StatsText)
        .insert(StatsTimer(Timer::from_seconds(0.5, TimerMode::Repeating)));

    commands
        .spawn(PolylineBundle {
            polyline: polylines.add(Polyline::default()),
            material: polyline_materials.add(PolylineMaterial {
                width: 1.0,
                color: Color::RED,
                depth_bias: -1.0,
                ..default()
            }),
            ..default()
        })
        .insert(TrianglesPolyline);

    commands
        .spawn(PolylineBundle {
            polyline: polylines.add(Polyline::default()),
            material: polyline_materials.add(PolylineMaterial {
                width: 1.5,
                color: Color::LIME_GREEN,
                depth_bias: -1.0,
                ..default()
            }),
            ..default()
        })
        .insert(PointsPolyline);
}

fn update_diagnostics_text(
    time: Res<Time>,
    diagnostics: Res<DiagnosticsStore>,
    mut timer_query: Query<&mut StatsTimer>,
    mut text_query: Query<&mut Text, With<StatsText>>,
) {
    let mut stats_text = text_query.single_mut();

    if !timer_query.single_mut().tick(time.delta()).just_finished() {
        return;
    }

    if let Some(fps_diagnostic) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
        if let Some(fps) = fps_diagnostic.value() {
            stats_text.sections[1].value = format!("{fps:.0}");
        }
    }

    if let Some(orbits_diagnostic) =
        diagnostics.get(OrbitTessellationDiagnosticsPlugin::ORBIT_COUNT)
    {
        if let Some(orbits) = orbits_diagnostic.value() {
            stats_text.sections[3].value = format!("{orbits:0}");
        }
    }

    if let Some(initial_points_diagnostic) =
        diagnostics.get(OrbitTessellationDiagnosticsPlugin::INITIAL_POINTS_COUNT)
    {
        if let Some(initial_points) = initial_points_diagnostic.value() {
            stats_text.sections[5].value = format!("{initial_points:0}");
        }
    }

    if let Some(points_diagnostic) =
        diagnostics.get(OrbitTessellationDiagnosticsPlugin::POINTS_COUNT)
    {
        if let Some(points) = points_diagnostic.value() {
            stats_text.sections[7].value = format!("{points:0}");
        }
    }
}

fn draw_triangles(
    mut polylines: ResMut<Assets<Polyline>>,
    settings: Res<DebugSettings>,
    orbit_polyline_query: Query<(&Handle<Polyline>, &TessellationSettings, &GlobalTransform)>,
    triangles_polyline_query: Query<&Handle<Polyline>, With<TrianglesPolyline>>,
) {
    let mut line_vertices = vec![];

    if settings.show_triangles {
        for (orbit_polyline_handle, tessellation_settings, global_transform) in
            orbit_polyline_query.iter()
        {
            let orbit_polyline = polylines.get(orbit_polyline_handle).cloned().unwrap();
            let vertices = &orbit_polyline.vertices;

            let vertices_next = vertices.iter().chain(vertices).skip(1).take(vertices.len());
            let vertices_next_next = vertices.iter().chain(vertices).skip(2).take(vertices.len());

            vertices
                .iter()
                .zip(vertices_next)
                .zip(vertices_next_next)
                .for_each(|((a, b), c)| {
                    let a = global_transform.transform_point(*a);
                    let b = global_transform.transform_point(*b);
                    let c = global_transform.transform_point(*c);
                    let area = 0.5 * (b - a).cross(c - b).length();

                    if area > tessellation_settings.max_area {
                        line_vertices.extend_from_slice(&[a, c, Vec3::splat(f32::INFINITY)]);
                    }
                });
        }
    }

    polylines
        .get_mut(triangles_polyline_query.single())
        .unwrap()
        .vertices = line_vertices;
}

fn draw_points(
    mut polylines: ResMut<Assets<Polyline>>,
    settings: Res<DebugSettings>,
    orbit_polyline_query: Query<(&Handle<Polyline>, &GlobalTransform), With<TessellationSettings>>,
    points_polyline_query: Query<&Handle<Polyline>, With<PointsPolyline>>,
) {
    let mut line_vertices = vec![];

    if settings.show_points {
        for (orbit_polyline_handle, global_transform) in orbit_polyline_query.iter() {
            let orbit_polyline = polylines.get(orbit_polyline_handle).cloned().unwrap();
            let vertices = &orbit_polyline.vertices;

            vertices.iter().for_each(|vertex| {
                let vertex = global_transform.transform_point(*vertex);
                let point_transform =
                    Transform::from_translation(vertex).with_scale(Vec3::splat(3000000000.0));
                let point_vertices = POINT_VERTICES.map(|v| point_transform * v);
                line_vertices.extend_from_slice(&point_vertices);
                line_vertices.push(Vec3::splat(f32::INFINITY));
            });
        }
    }

    polylines
        .get_mut(points_polyline_query.single())
        .unwrap()
        .vertices = line_vertices;
}
