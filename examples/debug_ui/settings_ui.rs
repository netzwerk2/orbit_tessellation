use bevy::math::DVec3;
use bevy::prelude::*;
use bevy_egui::egui::epaint::Shadow;
use bevy_egui::egui::{Grid, Slider, Visuals, Window as EguiWindow};
use bevy_egui::{EguiContexts, EguiPlugin};

use orbit_tessellation::{Orbit, StateVectors, TessellationSettings};

pub struct SettingsUiPlugin;

impl Plugin for SettingsUiPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<DebugSettings>()
            .add_plugins(EguiPlugin)
            .add_systems(Update, (insert_state_vectors, settings_ui));
    }
}

#[derive(Component, Deref, DerefMut)]
struct Position(DVec3);

#[derive(Component, Deref, DerefMut)]
struct Velocity(DVec3);

fn insert_state_vectors(
    mut commands: Commands,
    orbit_query: Query<(Entity, &Orbit), Added<Orbit>>,
) {
    for (entity, orbit) in orbit_query.iter() {
        let state_vectors = orbit.calculate_state_vectors(0.0);

        commands
            .entity(entity)
            .insert(Position(state_vectors.position))
            .insert(Velocity(state_vectors.velocity));
    }
}

#[derive(Default, Resource)]
pub struct DebugSettings {
    pub show_triangles: bool,
    pub show_points: bool,
}

fn settings_ui(
    mut egui_context: EguiContexts,
    mut settings: ResMut<DebugSettings>,
    mut orbit_query: Query<(
        &mut TessellationSettings,
        &mut Position,
        &mut Velocity,
        &mut Orbit,
    )>,
) {
    let visuals = Visuals {
        window_shadow: Shadow::default(),
        ..Visuals::dark()
    };

    EguiWindow::new("Debug Settings")
        .fixed_pos((10.0, 10.0))
        .resizable(false)
        .show(egui_context.ctx_mut(), |ui| {
            ui.ctx().set_visuals(visuals);

            Grid::new("Debug Settings")
                .num_columns(2)
                .spacing((10.0, 10.0))
                .show(ui, |ui| {
                    ui.label("Show Triangles:");
                    ui.checkbox(&mut settings.show_triangles, "");
                    ui.end_row();
                    ui.label("Show Points");
                    ui.checkbox(&mut settings.show_points, "");
                    ui.end_row();
                });

            ui.separator();

            for (i, (mut tessellation_settings, mut position, mut velocity, mut orbit)) in
                orbit_query.iter_mut().enumerate()
            {
                let TessellationSettings {
                    initial_points: ref mut min_points,
                    ref mut max_area,
                    ref mut max_iterations,
                } = *tessellation_settings;

                ui.collapsing(format!("Orbit {i}"), |ui| {
                    Grid::new(format!("Orbit {i} Settings"))
                        .num_columns(2)
                        .spacing((10.0, 10.0))
                        .show(ui, |ui| {
                            ui.label("Min Points");
                            ui.add(Slider::new(min_points, 3..=128));
                            ui.end_row();
                            ui.label("Max Area");
                            ui.add(Slider::new(max_area, 10f32..=10000f32));
                            ui.end_row();
                            ui.label("Max Iterations");
                            ui.add(Slider::new(max_iterations, 0..=8));
                            ui.end_row();
                            ui.label("Initial Position");
                            ui.vertical(|ui| {
                                ui.add(Slider::new(&mut position.x, 0f64..=20e9));
                                ui.add(Slider::new(&mut position.y, 0f64..=20e9));
                                ui.add(Slider::new(&mut position.z, 0f64..=20e9));
                            });
                            ui.end_row();
                            ui.label("Initial Velocity");
                            ui.vertical(|ui| {
                                ui.add(Slider::new(&mut velocity.x, 0f64..=10e3));
                                ui.add(Slider::new(&mut velocity.y, 0f64..=10e3));
                                ui.add(Slider::new(&mut velocity.z, 0f64..=10e3));
                            });
                            ui.end_row();
                        });
                });

                *orbit = Orbit::from_state_vectors(
                    StateVectors::new(position.0, velocity.0),
                    orbit.gravitational_parameter,
                    orbit.time,
                );
            }
        });
}
