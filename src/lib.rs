//! This crates allows users to draw orbits and optionally tessellate them beforehand
//! to make them look smoother.

use bevy::prelude::*;
pub use bevy_polyline;
use bevy_polyline::prelude::*;

pub use orbit::{Orbit, StateVectors, TessellationSettings};
pub use orbit_diagnostics::OrbitTessellationDiagnosticsPlugin;
pub use orbit_plugin::{DrawOrbitPlugin, OrbitBundle, PolylineMaterial, TessellationCamera};

mod orbit;
mod orbit_diagnostics;
mod orbit_plugin;

/// A plugin for rendering and tessellating orbits.
pub struct OrbitTessellationPlugin;

impl Plugin for OrbitTessellationPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((PolylinePlugin, DrawOrbitPlugin));
    }
}
