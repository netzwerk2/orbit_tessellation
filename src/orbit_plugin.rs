//! Tessellate and draw orbits.

use std::sync::{Arc, Mutex};

use bevy::prelude::*;
use bevy::render::camera::{CameraProjection, Projection};
use bevy::window::PrimaryWindow;
pub use bevy_polyline::material::PolylineMaterial;
// This import is needed for the link in the doc string to work.
#[cfg(doc)]
use bevy_polyline::polyline::PolylineBundle;
use bevy_polyline::prelude::*;

use crate::orbit::TessellationSettings;
use crate::Orbit;

/// A plugin for rendering orbits.
pub struct DrawOrbitPlugin;

impl Plugin for DrawOrbitPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(PostUpdate, draw_orbit);
    }
}

// TODO: Add GlobalTransform, Visibility, etc.
/// A component bundle for orbit entities.
#[derive(Bundle, Default)]
pub struct OrbitBundle {
    /// The tessellation settings for this specific orbit.
    pub tessellation_settings: TessellationSettings,
    /// The polyline bundle used for drawing the orbit.
    pub polyline_bundle: PolylineBundle,
    /// The actual description of this orbit.
    pub orbit: Orbit,
}

/// A marker component for the camera that is used for tessellation.
#[derive(Component, Copy, Clone, Debug)]
pub struct TessellationCamera;

/// Tessellate and draw the orbits.
///
/// The tessellation only happens if either the [`TessellationSettings`], the [`Orbit`],
/// the transform or the camera's transform change.
#[allow(clippy::type_complexity)]
fn draw_orbit(
    polylines: ResMut<Assets<Polyline>>,
    window_query: Query<&Window, With<PrimaryWindow>>,
    mut orbit_query: Query<(
        &TessellationSettings,
        &Orbit,
        &Handle<Polyline>,
        &GlobalTransform,
        (
            Ref<Orbit>,
            Ref<TessellationSettings>,
            Ref<Handle<Polyline>>,
            Ref<GlobalTransform>,
        ),
    )>,
    // TODO: Also register camera projection changes.
    transform_query: Query<
        (&Projection, &GlobalTransform, Ref<GlobalTransform>),
        With<TessellationCamera>,
    >,
) {
    let window = window_query.single();
    let window_size = Vec2::new(window.width(), window.height());

    let polylines = Arc::new(Mutex::new(polylines));

    if let Ok((projection, transform, transform_tracker)) = transform_query.get_single() {
        orbit_query.par_iter_mut().for_each(
            |(tessellation_settings, orbit, polyline_handle, global_transform, orbit_trackers)| {
                if !(transform_tracker.is_changed()
                    || orbit_trackers.0.is_changed()
                    || orbit_trackers.1.is_changed()
                    || orbit_trackers.2.is_changed()
                    || orbit_trackers.3.is_changed())
                {
                    return;
                }

                let vertices = orbit.tessellate(
                    *tessellation_settings,
                    window_size,
                    projection.get_projection_matrix() * transform.compute_matrix().inverse(),
                    global_transform,
                );

                polylines
                    .lock()
                    .unwrap()
                    .get_mut(polyline_handle)
                    .unwrap()
                    .vertices = vertices.into_iter().map(|v| v.as_vec3()).collect();
            },
        );
    }
}
