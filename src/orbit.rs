//! Structs used for calculations involving orbits.

use std::f64::consts::{FRAC_PI_2, TAU};

use bevy::math::{DMat3, DVec3};
use bevy::prelude::*;

/// A keplerian orbit specified by its [orbital elements](https://en.wikipedia.org/wiki/Orbital_elements).
#[derive(Copy, Clone, Debug, Component)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Orbit {
    /// The orbit's [semi-major axis](https://en.wikipedia.org/wiki/Semi-major_axis) a.
    ///
    /// It's half the sum of the distance between apoapsis and periapsis.
    pub semi_major_axis: f64,
    /// The orbit's [eccentricity](https://en.wikipedia.org/wiki/Orbital_eccentricity) e.
    ///
    /// It describes the shape of the orbit
    /// - e = 0:     circular orbit
    /// - 0 < e < 1: elliptic orbit
    /// - e = 1:     parabolic trajectory
    /// - e > 1:     hyperbolic trajectory
    pub eccentricity: f64,
    /// The orbit's [inclination](https://en.wikipedia.org/wiki/Orbital_inclination) i.
    ///
    /// It describes the angle between the orbit and the reference plane.
    pub inclination: f64,
    /// The orbit's [longitude of ascending node](https://en.wikipedia.org/wiki/Longitude_of_the_ascending_node) Ω.
    ///
    /// It describes the angle between the ascending node and the reference direction.
    pub longitude_of_ascending_node: f64,
    /// The orbit's [argument of periapsis](https://en.wikipedia.org/wiki/Argument_of_periapsis) ω.
    ///
    /// It describes the angle between the ascending node and the periapsis.
    pub argument_of_periapsis: f64,
    /// The orbit's [mean anomaly](https://en.wikipedia.org/wiki/Mean_anomaly) M.
    ///
    /// It describes the position of the orbiting body.
    pub mean_anomaly: f64,
    /// The orbit's [standard gravitational parameter](https://en.wikipedia.org/wiki/Standard_gravitational_parameter) μ.
    pub gravitational_parameter: f64,
    /// The orbit's time, used to calculate [`StateVectors`].
    ///
    /// It's only useful to specify a time different from 0
    /// if you have multiple orbits, where the current positions don't refer to the same point in time.
    pub time: f64,
}

// TODO: Change/Remove default value
impl Default for Orbit {
    fn default() -> Self {
        let position = DVec3::new(
            -5.31491441583025E+10,
            -7.17286737381666E+06,
            1.37188340588073E+11,
        );
        let velocity = DVec3::new(
            -2.82578511067815E+04,
            1.35281602274339E00,
            -1.08635987330246E+04,
        );

        Self::from_state_vectors(StateVectors::new(position, velocity), 1.32712440018e20, 0.0)
    }
}

impl Orbit {
    /// Create a new orbit.
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        semi_major_axis: f64,
        eccentricity: f64,
        inclination: f64,
        longitude_of_ascending_node: f64,
        argument_of_periapsis: f64,
        mean_anomaly: f64,
        gravitational_parameter: f64,
        time: f64,
    ) -> Self {
        Self {
            semi_major_axis,
            eccentricity,
            inclination,
            longitude_of_ascending_node,
            argument_of_periapsis,
            mean_anomaly,
            gravitational_parameter,
            time,
        }
    }

    /// Calculate the eccentricity vector when converting from state vectors to orbital parameters.
    fn eccentricity_vector(
        state_vectors: StateVectors,
        specific_angular_momentum: DVec3,
        gravitational_parameter: f64,
    ) -> DVec3 {
        state_vectors.velocity.cross(specific_angular_momentum) / gravitational_parameter
            - state_vectors.position.normalize()
    }

    fn true_anomaly(eccentricity_vector: DVec3, state_vectors: StateVectors) -> f64 {
        let true_anomaly_cos = eccentricity_vector.dot(state_vectors.position)
            / (eccentricity_vector.length() * state_vectors.position.length());
        let true_anomaly = true_anomaly_cos.clamp(-1.0, 1.0).acos();

        if state_vectors.position.dot(state_vectors.velocity) >= 0.0 {
            true_anomaly
        } else {
            TAU - true_anomaly
        }
    }

    /// Calculate the inclination when converting from state vectors to orbital parameters.
    fn inclination(specific_angular_momentum: DVec3) -> f64 {
        let inclination_cos = specific_angular_momentum.z / specific_angular_momentum.length();

        inclination_cos.clamp(-1.0, 1.0).acos()
    }

    fn eccentric_anomaly(true_anomaly: f64, eccentricity: f64) -> f64 {
        2.0 * ((0.5 * true_anomaly).tan() / ((1.0 + eccentricity) / (1.0 - eccentricity)).sqrt())
            .atan()
    }

    /// Calculate the longitude of the ascending node when converting from state vectors to orbital parameters.
    fn longitude_of_ascending_node(to_ascending_node: DVec3) -> f64 {
        if to_ascending_node == DVec3::ZERO {
            0.0
        } else {
            let ascending_cos = to_ascending_node.x / to_ascending_node.length();
            let longitude_of_ascending_node = ascending_cos.clamp(-1.0, 1.0).acos();

            if to_ascending_node.y <= 0.0 {
                longitude_of_ascending_node
            } else {
                TAU - longitude_of_ascending_node
            }
        }
    }

    /// Calculate the argument of periapsis when converting from state vectors to orbital parameters.
    fn argument_of_periapsis(to_ascending_node: DVec3, eccentricity_vector: DVec3) -> f64 {
        if to_ascending_node == DVec3::ZERO || eccentricity_vector == DVec3::ZERO {
            0.0
        } else {
            let periapsis_cos = to_ascending_node.dot(eccentricity_vector)
                / (to_ascending_node.length() * eccentricity_vector.length());
            let argument_of_periapsis = periapsis_cos.clamp(-1.0, 1.0).acos();

            if eccentricity_vector.z <= 0.0 {
                argument_of_periapsis
            } else {
                TAU - argument_of_periapsis
            }
        }
    }

    /// Calculate the mean anomaly when converting from state vectors to orbital parameters.
    fn mean_anomaly(eccentricity: f64, eccentric_anomaly: f64) -> f64 {
        eccentric_anomaly - eccentricity * eccentric_anomaly.sin()
    }

    /// Calculate the semi-major axis when converting from state vectors to orbital parameters.
    fn semi_major_axis(state_vectors: StateVectors, gravitational_parameter: f64) -> f64 {
        1.0 / (2.0 / state_vectors.position.length()
            - state_vectors.velocity.length_squared() / gravitational_parameter)
    }

    // TODO: Support non-elliptical orbits
    // TODO: Calculate time from mean_anomaly
    /// Calculate the orbital elements of an orbit from its [`StateVectors`]
    /// standard gravitational parameter and time.
    ///
    /// The implementation is based on
    /// [this document](https://downloads.rene-schwarz.com/download/M002-Cartesian_State_Vectors_to_Keplerian_Orbit_Elements.pdf)
    /// by René Schwarz.
    pub fn from_state_vectors(
        mut state_vectors: StateVectors,
        gravitational_parameter: f64,
        time: f64,
    ) -> Self {
        state_vectors = state_vectors.convert_to_physics();

        let position = state_vectors.position;
        let velocity = state_vectors.velocity;

        let specific_angular_momentum = position.cross(velocity);

        let eccentricity_vector = Self::eccentricity_vector(
            state_vectors,
            specific_angular_momentum,
            gravitational_parameter,
        );

        let to_ascending_node = DVec3::new(
            -specific_angular_momentum.y,
            specific_angular_momentum.x,
            0.0,
        );
        let true_anomaly = Self::true_anomaly(eccentricity_vector, state_vectors);

        let inclination = Self::inclination(specific_angular_momentum);

        let eccentricity = eccentricity_vector.length();
        let eccentric_anomaly = Self::eccentric_anomaly(true_anomaly, eccentricity);

        let longitude_of_ascending_node = Self::longitude_of_ascending_node(to_ascending_node);

        let argument_of_periapsis =
            Self::argument_of_periapsis(to_ascending_node, eccentricity_vector);

        let mean_anomaly = Self::mean_anomaly(eccentricity, eccentric_anomaly);

        let semi_major_axis = Self::semi_major_axis(state_vectors, gravitational_parameter);

        Self {
            semi_major_axis,
            eccentricity,
            inclination,
            longitude_of_ascending_node,
            argument_of_periapsis,
            mean_anomaly,
            gravitational_parameter,
            time,
        }
    }

    /// Return the orbital period of this orbit.
    pub fn period(&self) -> f64 {
        TAU * (self.semi_major_axis.powi(3) / self.gravitational_parameter).sqrt()
    }

    /// Calculate the eccentric anomaly using Newton–Raphson iteration.
    ///
    /// The iteration stops when the difference to the last iteration is smaller than 10⁻¹².
    fn calculate_eccentric_anomaly(&self, mean_anomaly: f64) -> f64 {
        let mut eccentric_anomaly = mean_anomaly;
        let mut correction = f64::INFINITY;

        while correction.abs() > 1e-12 {
            correction =
                (eccentric_anomaly - self.eccentricity * eccentric_anomaly.sin() - mean_anomaly)
                    / (1.0 - self.eccentricity * eccentric_anomaly.cos());
            eccentric_anomaly -= correction;
        }

        eccentric_anomaly
    }

    /// Calculate the [`StateVectors`] of the orbit at a given point in time.
    /// The time is relative to the same reference point as the orbit's time.
    /// This means that the orbit's time is subtracted from the given time to get Δt.
    ///
    /// The implementation is based on
    /// [this document](https://downloads.rene-schwarz.com/download/M001-Keplerian_Orbit_Elements_to_Cartesian_State_Vectors.pdf)
    /// by René Schwarz.
    pub fn calculate_state_vectors(&self, time: f64) -> StateVectors {
        let delta_time = time - self.time;

        let mean_anomaly = (self.mean_anomaly
            + delta_time * (self.gravitational_parameter * self.semi_major_axis.powi(-3)).sqrt())
        .rem_euclid(TAU);

        let eccentric_anomaly = self.calculate_eccentric_anomaly(mean_anomaly);

        let true_anomaly = 2.0
            * ((1.0 + self.eccentricity).sqrt() * (0.5 * eccentric_anomaly).sin())
                .atan2((1.0 - self.eccentricity).sqrt() * (0.5 * eccentric_anomaly).cos());

        let distance = self.semi_major_axis * (1.0 - self.eccentricity * eccentric_anomaly.cos());

        let local_position = distance * DVec3::new(true_anomaly.cos(), true_anomaly.sin(), 0.0);
        let local_velocity = DVec3::new(
            -eccentric_anomaly.sin(),
            (1.0 - self.eccentricity.powi(2)).sqrt() * eccentric_anomaly.cos(),
            0.0,
        ) * (self.gravitational_parameter * self.semi_major_axis).sqrt()
            / distance;

        let rotation = DMat3::from_rotation_z(-self.longitude_of_ascending_node)
            * DMat3::from_rotation_x(-self.inclination)
            * DMat3::from_rotation_z(-self.argument_of_periapsis);

        let position = rotation * local_position;
        let velocity = rotation * local_velocity;

        StateVectors::new(position, velocity).convert_to_bevy()
    }

    pub fn calculate_position(&self, time: f64) -> DVec3 {
        self.calculate_state_vectors(time).position
    }

    /// Generate the initial points to tessellate.
    ///
    /// Currently the points are sampled uniformly in respect to time.
    /// However, it would be more efficient to uniformly sample the points
    /// in respect to their mean anomaly, since a body is faster at periapsis than at apoapsis.
    fn generate_initial_points(
        &self,
        step: f64,
        min_points: usize,
        parameters: &mut Vec<f64>,
    ) -> Vec<DVec3> {
        (0..min_points)
            .map(|i| {
                let t = i as f64 * step;
                parameters.push(t);

                self.calculate_state_vectors(t).position
            })
            .collect::<Vec<DVec3>>()
    }

    /// Transform a point in 3D space to screen space.
    ///
    /// If the transformed point is outside of the screen, it's still returned.
    fn world_to_screen(
        position: Vec3,
        window_size: Vec2,
        camera_matrix: Mat4,
        global_transform: &GlobalTransform,
    ) -> Vec2 {
        let ndc_coords = camera_matrix.project_point3(global_transform.transform_point(position));

        (ndc_coords.truncate() + Vec2::ONE) * 0.5 * window_size
    }

    // TODO: Viewed from very small angles the tessellation isn't working correctly.
    /// Tessellate the orbit based on the given [`TessellationSettings`].
    ///
    /// The principle is quite simple and originates from a
    /// [KSP 2 dev diary](https://forum.kerbalspaceprogram.com/index.php?/topic/201736-developer-insights-9-%E2%80%93-orbit-tessellation/).
    /// It works by iterating over the triangles formed by triples of neighbouring points.
    /// If any of the 3 points of the current triangle is visible it possibly needs to be tessellated.
    /// To actually determine if a triangle should be tessellated we use its area.
    /// If it's greater than the specified maximum area in the [`TessellationSettings`],
    /// the two orbit edges are tessellated.
    pub fn tessellate(
        &self,
        tessellation_settings: TessellationSettings,
        window_size: Vec2,
        camera_matrix: Mat4,
        global_transform: &GlobalTransform,
    ) -> Vec<DVec3> {
        let TessellationSettings {
            initial_points: min_points,
            max_area,
            max_iterations,
        } = tessellation_settings;

        let mut step = self.period() / min_points as f64;

        let mut parameters = vec![];
        let mut vertices = self.generate_initial_points(step, min_points, &mut parameters);

        let mut tessellated = true;

        for _ in 0..max_iterations {
            if !tessellated {
                break;
            }

            tessellated = false;
            step *= 0.5;

            let vertices_next = vertices
                .iter()
                .chain(vertices.iter())
                .skip(1)
                .take(vertices.len());
            let vertices_next_next = vertices
                .iter()
                .chain(vertices.iter())
                .skip(2)
                .take(vertices.len());

            // This vector keeps tracks of the edges which need to be further tessellated.
            let mut tessellation_indices = vec![];

            // Iterate over all point triples (i.e. triangles).
            vertices
                .iter()
                .zip(vertices_next)
                .zip(vertices_next_next)
                .enumerate()
                .for_each(|(i, ((a, b), c))| {
                    let on_screen = |position: Vec2| -> bool {
                        (0f32..window_size.x).contains(&position.x)
                            && (0f32..window_size.y).contains(&position.y)
                    };

                    let a_screen = Self::world_to_screen(
                        a.as_vec3(),
                        window_size,
                        camera_matrix,
                        global_transform,
                    );
                    let b_screen = Self::world_to_screen(
                        b.as_vec3(),
                        window_size,
                        camera_matrix,
                        global_transform,
                    );
                    let c_screen = Self::world_to_screen(
                        c.as_vec3(),
                        window_size,
                        camera_matrix,
                        global_transform,
                    );

                    if !(on_screen(a_screen) || on_screen(b_screen) || on_screen(c_screen)) {
                        return;
                    }

                    let edge1 = b_screen - a_screen;
                    let edge2 = c_screen - b_screen;

                    let area = (0.5 * (edge1.x * edge2.y - edge1.y * edge2.x)).abs();

                    if area > max_area {
                        tessellated = true;
                        // If the last index isn't the current one, we skipped at least one triangle.
                        // That means the triangle's first edge isn't pushed already,
                        // thus we need to push it.
                        if let Some(last) = tessellation_indices.last() {
                            if *last != i {
                                tessellation_indices.push(i);
                            }
                        } else {
                            // This is the first triangle so we need to push its first edge.
                            tessellation_indices.push(i);
                        }

                        let second_edge_index = (i + 1) % vertices.len();

                        // If this is the very last point,
                        // the triangle's second edge is the very first edge of the orbit.
                        // If it has already been added to the tessellation indices,
                        // we don't push it another time.
                        if tessellation_indices[0] != second_edge_index {
                            tessellation_indices.push(second_edge_index);
                        }
                    }
                });

            tessellation_indices
                .into_iter()
                .enumerate()
                .for_each(|(i, edge_index)| {
                    // `i` is added to `edge_index` to compensate for the parameters which have already been inserted.
                    let t = parameters[edge_index + i] + step;

                    vertices.insert(edge_index + i + 1, self.calculate_state_vectors(t).position);
                    parameters.insert(edge_index + i + 1, t);
                });
        }

        // Close the orbit.
        vertices.push(vertices[0]);

        vertices
    }
}

/// The settings used to control the tessellation process.
#[derive(Component, Copy, Clone, Debug)]
pub struct TessellationSettings {
    /// The initial amount of points generated for the orbit.
    pub initial_points: usize,
    // TODO: Account for window size
    /// The maximum area a triangle is allowed to have before being tessellated.
    pub max_area: f32,
    /// The maximum amount of times the orbit is tessellated.
    ///
    /// If there is no tessellation left to do, the tessellation will not iterate further.
    pub max_iterations: usize,
}

impl Default for TessellationSettings {
    fn default() -> Self {
        Self {
            initial_points: 64,
            max_area: 50.0,
            max_iterations: 4,
        }
    }
}

/// The orbital [state vectors](https://en.wikipedia.org/wiki/Orbital_state_vectors) of an orbit.
#[derive(Copy, Clone, Debug)]
pub struct StateVectors {
    /// The orbit's current position.
    pub position: DVec3,
    /// The orbit's current velocity.
    pub velocity: DVec3,
}

impl From<(DVec3, DVec3)> for StateVectors {
    fn from(v: (DVec3, DVec3)) -> Self {
        Self::new(v.0, v.1)
    }
}

impl StateVectors {
    /// Create new state vectors.
    pub fn new(position: DVec3, velocity: DVec3) -> Self {
        Self { position, velocity }
    }

    pub fn convert_to_bevy(&self) -> Self {
        let transform = DMat3::from_rotation_x(FRAC_PI_2);

        Self::new(
            transform * self.position * DVec3::new(1.0, -1.0, 1.0),
            transform * self.velocity * DVec3::new(1.0, -1.0, 1.0),
        )
    }

    pub fn convert_to_physics(&self) -> Self {
        let transform = DMat3::from_rotation_x(-FRAC_PI_2);

        Self::new(transform * self.position, transform * self.velocity)
    }
}
