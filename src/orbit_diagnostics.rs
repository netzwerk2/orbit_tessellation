//! Diagnostics for orbit tessellation.

use bevy::diagnostic::{Diagnostic, DiagnosticId, Diagnostics, RegisterDiagnostic};
use bevy::prelude::*;
use bevy_polyline::prelude::*;

use crate::TessellationSettings;

// TODO: Extra feature
/// A plugin to add diagnostics for orbit tessellation.
pub struct OrbitTessellationDiagnosticsPlugin;

impl Plugin for OrbitTessellationDiagnosticsPlugin {
    fn build(&self, app: &mut App) {
        app.register_diagnostic(Diagnostic::new(Self::ORBIT_COUNT, "orbit_count", 1))
            .register_diagnostic(Diagnostic::new(
                Self::INITIAL_POINTS_COUNT,
                "initial_points_count",
                1,
            ))
            .register_diagnostic(Diagnostic::new(Self::POINTS_COUNT, "points_count", 1))
            .add_systems(Update, Self::diagnostic_system);
    }
}

impl OrbitTessellationDiagnosticsPlugin {
    /// The amount of orbits.
    pub const ORBIT_COUNT: DiagnosticId =
        DiagnosticId::from_u128(47085790754221643294573805837763363675);
    /// The amount of initial points generated for orbits.
    pub const INITIAL_POINTS_COUNT: DiagnosticId =
        DiagnosticId::from_u128(256388475499699288161857067537700732697);
    /// The total amount of points generated for orbits, i.e. after the tessellation process.
    pub const POINTS_COUNT: DiagnosticId =
        DiagnosticId::from_u128(298237235885219283366938059130804892577);

    /// Update the three diagnostics.
    pub fn diagnostic_system(
        mut diagnostics: Diagnostics,
        orbits_query: Query<(&Handle<Polyline>, &TessellationSettings)>,
        polylines: Res<Assets<Polyline>>,
    ) {
        let (orbit_count, initial_points_count, points_count) = orbits_query.iter().fold(
            (0, 0, 0),
            |(orbit_count, initial_points_count, points_count),
             (polyline_handle, tessellation_settings)| {
                (
                    orbit_count + 1,
                    initial_points_count + tessellation_settings.initial_points,
                    points_count + polylines.get(polyline_handle).unwrap().vertices.len(),
                )
            },
        );

        diagnostics.add_measurement(Self::ORBIT_COUNT, || orbit_count as f64);
        diagnostics.add_measurement(Self::INITIAL_POINTS_COUNT, || initial_points_count as f64);
        diagnostics.add_measurement(Self::POINTS_COUNT, || points_count as f64);
    }
}
